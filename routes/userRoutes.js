// dependencies
const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Register User
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Check Email
router.post("/checkEmail", userController.checkEmail)

// Login User
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Create Order
router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	userController.createOrder(data).then(
		resultFromController => res.send(resultFromController))
});



// Retrieve User details
router.get("/details", (req, res) => {
	const token = req.headers.authorization
	console.log(token)
	console.log(typeof token == "undefined")
	if(typeof token == "undefined"){
		return res.send({})
	} 
	
	const userDetails = auth.decode(token)
	console.log(userDetails)
	


	return userController.getDetails(userDetails.id).then(
		resultFromController => res.send(resultFromController))
})


module.exports = router;