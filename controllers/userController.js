const User = require("../models/user.js"); // returns objects, literal and function, not the whole document.
const Product = require("../models/product.js");
const bcrypt = require("bcrypt"); // bcrypt - package for password hashing
const auth = require("../auth.js");
const user = require("../models/user.js");

// Register User
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    mobileNo: reqBody.mobileNo,
    isAdmin: reqBody.isAdmin,
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.checkEmail = (req, res) => {
  const email = req.body.email;
  console.log(email);

  req.body.email;

  User.find({ email: email }).then((result) => {
    console.log(result);
    if (result.length == 0) {
      return res.send(false);
    }

    return res.send(true);
  });
};

// Login User
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      // compareSync is bcrypt function to compare a hashed password to unhashed password
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        // if password does not match
        return false;
        // return "Incorrect password"
      }
    }
  });
};

// Create Order
module.exports.createOrder = (data) => {
  const userId = data.userId;
  if (typeof data.isAdmin === true && data.isAdmin) {
    return "Cannot checkout item";
  } else {
    console.log(userId);
    const orderObj = {
      products: [{ productName: data.product.name, quantity: 1 }],
      totalAmount: data.product.price,
    };

    User.updateMany(
      { _id: userId },
      {
        $push: {
          orders: orderObj
        },
      }
    ).then((result, err) => {
    });

    // orders.push({
    //   products: [{ productName: data.product.name, quantity: 1 }],
    //   totalAmount: data.product.price,
    // });
    // User.save();
    const returnPromise = new Promise((resolve, reject) => {
      resolve(true);
    });

    return returnPromise;

    // let newProduct = new Product({
    //   name: data.product.name,
    //   description: data.product.description,
    //   price: data.product.price
    // });

    // return newProduct.save().then((newProduct, error) => {
    //   if (error) {
    //     return error;
    //   }
    //   return newProduct;
    // });
  }
};

// Retrieve User details

module.exports.getDetails = (userId) => {
  // const token = req.headers.authorization
  console.log(userId);
  return User.findById(userId).then((result) => {
    console.log(result);
    return result;
  });
};
