const mongoose = require("mongoose");
mongoose.set('strictQuery', true)

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    orders :[
        {
            products:[ {
                    productName: {
                        type: String,
                        required: [true, "productName is required"]
                    },
                    quantity: {
                        type: Number,
                        required: [true, "quantity is required"]
                    }
                }],
            totalAmount: {
                    type: Number,
                    required: [true, "totalAmount is required"]
            },
            purchasedOn: {
                    type: Date,
                    default: new Date()
            }
        }
        ]
})

module.exports = mongoose.model("User", userSchema);
