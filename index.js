/*
Git commands:
	npm init -y
	npm install express mongoose
	touch .gitignore >> content node_modules
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");

// ROUTERS
const userRoutes = require ("./routes/userRoutes.js");
const productRoutes = require ("./routes/productRoutes.js");


// Server setup
const app = express();


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


// DB connection
mongoose.connect("mongodb://user:user@ac-nmp9yhz-shard-00-00.bu8e0iz.mongodb.net:27017,ac-nmp9yhz-shard-00-01.bu8e0iz.mongodb.net:27017,ac-nmp9yhz-shard-00-02.bu8e0iz.mongodb.net:27017/?ssl=true&replicaSet=atlas-v4ksaf-shard-0&authSource=admin&retryWrites=true&w=majority",

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}) .catch(error => { console.log(error)});

mongoose.connection.once('open', () => console.log('Now connected to Maranan-Mongo DB Atlas'));


app.listen(process.env.PORT || 4000, () => 
{console.log(`API is now online on port ${process.env.PORT || 4000}`)
});